

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";




CREATE TABLE IF NOT EXISTS `admin_login` (
  `id` int(11) NOT NULL,
  `admin_username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `admin_username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE IF NOT EXISTS `assignment` (
  `id` int(20) NOT NULL,
  `link` varchar(400) NOT NULL,
  `title` varchar(30) NOT NULL,
  `class` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`id`, `link`, `title`, `class`) VALUES
(5, 'assignment/field.docx', 'field trip', '1');


-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id` int(10) NOT NULL,
  `sec` varchar(10) NOT NULL,
  `classname` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `sec`, `classname`) VALUES
(6, 'A', 1),
(7, 'B', 1),
(8, 'A', 2),
(9, 'B', 2);


-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE IF NOT EXISTS `fees` (
  `id` int(20) NOT NULL,
  `adm_no` varchar(30) NOT NULL,
  `student_name` varchar(30) NOT NULL,
  `father_name` varchar(30) NOT NULL,
  `amount` int(20) NOT NULL,
  `month` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`id`, `adm_no`, `student_name`, `father_name`, `amount`, `month`) VALUES
(1, '001', 'ARVIND NANDA', 'ABCD ', 750, 'JUL'),
(2, '001', 'ARVIND NANDA', 'ABCD ', 750, 'JUN'),
(3, '002', 'SOURAV', 'BCDE ', 750, 'JUL'),
(4, '002', 'SOURAV', 'BCDE ', 250, 'JUN');

-- --------------------------------------------------------



-- --------------------------------------------------------

--
-- Table structure for table `student_attendance`
--

CREATE TABLE IF NOT EXISTS `student_attendance` (
  `id` int(20) NOT NULL,
  `class` text NOT NULL,
  `student_id` text NOT NULL,
  `student_name` text NOT NULL,
  `dat` text NOT NULL,
  `mont` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_attendance`
--

INSERT INTO `student_attendance` (`id`, `class`, `student_id`, `student_name`, `dat`, `mont`, `status`) VALUES
(1, '1', '001', 'ARVIND NANDA', '20', 'JULY', 'P'),
(2, '1', '002', 'SOURAV', '20', 'JULY', 'P');


-- --------------------------------------------------------

--
-- Table structure for table `student_login`
--

CREATE TABLE IF NOT EXISTS `student_login` (
  `student_id` int(10) NOT NULL,
  `pwd` varchar(50) NOT NULL DEFAULT '123456',
  `id` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_login`
--

INSERT INTO `student_login` (`student_id`, `pwd`, `id`) VALUES
(6, '001', '001'),
(7, '002', '002'),
(8, '003', '003'),
(9, '056', '056');

-- --------------------------------------------------------

--
-- Table structure for table `student_registration`
--

CREATE TABLE IF NOT EXISTS `student_registration` (
  `student_id` int(20) NOT NULL,
  `adm_no` varchar(150) NOT NULL,
  `student_name` varchar(50) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `date_of_admission` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `date_of_birth` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `category` varchar(25) NOT NULL,
  `class` varchar(25) NOT NULL,
  `sec` varchar(20) NOT NULL,
  `fmob_no` varchar(30) NOT NULL,
  `email` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_registration`
--

INSERT INTO `student_registration` (`student_id`, `adm_no`, `student_name`, `father_name`, `mother_name`, `date_of_admission`, `address`, `date_of_birth`, `gender`, `category`, `class`, `sec`, `fmob_no`, `email`) VALUES
(20, '001', 'ARVIND NANDA', 'ABCD', 'SEDF', '2/07/2019', '8 ABCD', '30/1/1998', 'Male', 'GENERAL', '1', 'A', '0123456789', ''),
(21, '002', 'SOURAV', 'BCDE', 'SEDG', '1/1/1998', '8 mp colony bikaner', '1/1/1998', 'Male', 'GENERAL', '1', 'A', '1234567890', '');


-- --------------------------------------------------------

--
-- Table structure for table `student_timetable`
--

CREATE TABLE IF NOT EXISTS `student_timetable` (
  `id` int(20) NOT NULL,
  `class` text NOT NULL,
  `time` text NOT NULL,
  `monday` text NOT NULL,
  `tuesday` text NOT NULL,
  `wednesday` text NOT NULL,
  `thursday` text NOT NULL,
  `friday` text NOT NULL,
  `saturday` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_timetable`
--

INSERT INTO `student_timetable` (`id`, `class`, `time`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`) VALUES
(1, '1', '9:00', 'COMPUTER', 'COMPUTER', 'COMPUTER', 'COMPUTER', 'COMPUTER', 'COMPUTER'),
(2, '1', '', '', '', '', '', '', ''),
(3, '', '', '', '', '', '', '', ''),
(4, '1', '10', 'ENGLISH', 'ENGLISH', 'ENGLISH', 'ENGLISH', 'ENGLISH', 'ENGLISH');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `subid` int(10) NOT NULL,
  `subject` varchar(240) NOT NULL,
  `class` varchar(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subid`, `subject`, `class`) VALUES
(8, 'MATH', '2'),
(7, 'ENGLISH', '2'),
(3, 'CSE', '1');


-- --------------------------------------------------------

--
-- Table structure for table `teacher_login`
--

CREATE TABLE IF NOT EXISTS `teacher_login` (
  `teacher_id` int(20) NOT NULL,
  `teacher_name` varchar(30) NOT NULL,
  `subject` varchar(30) NOT NULL,
  `classteacher` int(20) NOT NULL,
  `pwd` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_login`
--

INSERT INTO `teacher_login` (`teacher_id`, `teacher_name`, `subject`, `classteacher`, `pwd`) VALUES
(1, 'brian', 'COMPUTER', 1, 'brian');

-- --------------------------------------------------------

--
-- Table structure for table `video_tutorial`
--

CREATE TABLE IF NOT EXISTS `video_tutorial` (
  `video_id` int(10) NOT NULL,
  `class` varchar(25) NOT NULL,
  `video_url` varchar(50) NOT NULL,
  `subject` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video_tutorial`
--

INSERT INTO `video_tutorial` (`video_id`, `class`, `video_url`, `subject`) VALUES
(1, '1', 'http://www.w3schools.com/php/php_file_upload.asp', 'HINDI'),
(3, '1', 'http://www.ssconline.nic.in/tpform1.php?exam_code=', 'COMPUTER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignment`
--
ALTER TABLE `assignment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parent_login`
--


--
-- Indexes for table `student_attendance`
--
ALTER TABLE `student_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_login`
--
ALTER TABLE `student_login`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `student_registration`
--
ALTER TABLE `student_registration`
  ADD PRIMARY KEY (`student_id`),
  ADD UNIQUE KEY `adm_no` (`adm_no`),
  ADD FULLTEXT KEY `sec` (`sec`);

--
-- Indexes for table `student_timetable`
--
ALTER TABLE `student_timetable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subid`);

--
-- Indexes for table `teacher_login`
--
ALTER TABLE `teacher_login`
  ADD PRIMARY KEY (`teacher_id`);

--
-- Indexes for table `video_tutorial`
--
ALTER TABLE `video_tutorial`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `assignment`
--
ALTER TABLE `assignment`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `parent_login`
--

--
-- AUTO_INCREMENT for table `student_attendance`
--
ALTER TABLE `student_attendance`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `student_login`
--
ALTER TABLE `student_login`
  MODIFY `student_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `student_registration`
--
ALTER TABLE `student_registration`
  MODIFY `student_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `student_timetable`
--
ALTER TABLE `student_timetable`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subid` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `teacher_login`
--
ALTER TABLE `teacher_login`
  MODIFY `teacher_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `video_tutorial`
--
ALTER TABLE `video_tutorial`
  MODIFY `video_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

